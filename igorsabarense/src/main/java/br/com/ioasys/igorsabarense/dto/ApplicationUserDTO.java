package br.com.ioasys.igorsabarense.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Data
public class ApplicationUserDTO  {
    private Long id;
    Boolean active;
    private String username;
    private String password;
    private String name;
    private String email;
    private Set<RoleDTO> roles;
    private LocalDateTime creationDateTime;

}
