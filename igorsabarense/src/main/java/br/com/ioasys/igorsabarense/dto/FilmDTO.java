package br.com.ioasys.igorsabarense.dto;

import br.com.ioasys.igorsabarense.enumeration.GenreEnumeration;
import lombok.Data;

import java.util.List;

@Data
public class FilmDTO {
    private Long id;
    private String title;
    private GenreEnumeration genre;
    private String releaseYear;
    private List<PersonDTO> cast;
    private Long votes;
    private Double averageVote;
}
