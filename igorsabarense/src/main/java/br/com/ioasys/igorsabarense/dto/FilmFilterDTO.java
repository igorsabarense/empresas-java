package br.com.ioasys.igorsabarense.dto;

import br.com.ioasys.igorsabarense.enumeration.CastRoleEnumeration;
import br.com.ioasys.igorsabarense.enumeration.GenreEnumeration;
import lombok.Data;
import org.springframework.data.domain.Pageable;


@Data
public class FilmFilterDTO {
    Pageable pageable;
    CastRoleEnumeration castRole;
    GenreEnumeration genre;
    String filmTitle;
    String name;
}
