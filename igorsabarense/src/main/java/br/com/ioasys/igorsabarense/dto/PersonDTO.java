package br.com.ioasys.igorsabarense.dto;

import br.com.ioasys.igorsabarense.enumeration.CastRoleEnumeration;
import lombok.Data;

@Data
public class PersonDTO {
    private Long id;
    private String name;
    private String age;
    private CastRoleEnumeration role;
}
