package br.com.ioasys.igorsabarense.dto;

import lombok.Data;

@Data
public class RoleDTO {
    private Long id;
    private String name;
}
