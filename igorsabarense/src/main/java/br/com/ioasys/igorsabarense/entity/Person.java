package br.com.ioasys.igorsabarense.entity;

import br.com.ioasys.igorsabarense.enumeration.CastRoleEnumeration;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private String age;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private CastRoleEnumeration role;


}