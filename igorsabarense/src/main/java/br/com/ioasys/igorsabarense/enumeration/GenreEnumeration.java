package br.com.ioasys.igorsabarense.enumeration;

public enum GenreEnumeration {
    ACTION, HORROR, COMEDY , ROMANCE
}
