package br.com.ioasys.igorsabarense.enumeration;

public enum RatingEnum {
    ONE_STAR , TWO_STARS , THREE_STARS, FOUR_STARS , FIVE_STARS
}
