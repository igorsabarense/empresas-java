package br.com.ioasys.igorsabarense.repository;

import br.com.ioasys.igorsabarense.entity.ApplicationUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    ApplicationUser findByUsername(String username);
    ApplicationUser findByEmail(String email);

    @Query(value = "SELECT u FROM ApplicationUser u JOIN u.roles r WHERE r.id = 2")
    Page<ApplicationUser> findAdminUsers(Pageable pageable);

    @Query(value = "select * from application_user au " +
            "join users_roles ur ON au.id = ur.user_id " +
            "join roles r on ur.role_id  = r.id " +
            "where ur.role_id in(1) " +
            " and ur.user_id not in (select user_id from users_roles ur2 where ur2.role_id = 2)" +
            " and au.active = true", nativeQuery = true)
    Page<ApplicationUser> getActiveNonAdminUsers(Pageable pageable);
}