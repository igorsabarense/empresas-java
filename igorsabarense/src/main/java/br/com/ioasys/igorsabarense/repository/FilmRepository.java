package br.com.ioasys.igorsabarense.repository;

import br.com.ioasys.igorsabarense.entity.Film;
import br.com.ioasys.igorsabarense.enumeration.CastRoleEnumeration;
import br.com.ioasys.igorsabarense.enumeration.GenreEnumeration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FilmRepository extends JpaRepository<Film, Long>, JpaSpecificationExecutor<Film> {
    Film findByTitle(String title);
    Optional<Film> findById(Long id);
    List<Film> findByGenre(GenreEnumeration genre);
    List<Film> findByTitleAndPerson(String title, CastRoleEnumeration role);
    List<Film> findAllByOrderByTitleAsc();
}

