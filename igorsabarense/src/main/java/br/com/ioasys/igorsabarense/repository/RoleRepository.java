package br.com.ioasys.igorsabarense.repository;

import br.com.ioasys.igorsabarense.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Long>{
    Role findByName(String userRole);
}
