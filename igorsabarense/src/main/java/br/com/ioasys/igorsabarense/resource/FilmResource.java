package br.com.ioasys.igorsabarense.resource;

import br.com.ioasys.igorsabarense.dto.FilmDTO;
import br.com.ioasys.igorsabarense.dto.FilmFilterDTO;
import br.com.ioasys.igorsabarense.entity.Film;
import br.com.ioasys.igorsabarense.enumeration.RatingEnum;
import br.com.ioasys.igorsabarense.service.facade.Facade;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/film")
public class FilmResource {

    private Boolean hasAdminAuthority(){
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ADMIN");
    }

    @Autowired
    private Facade facade;

    @ApiOperation(value = "Film creation; Only admins can use this feature.")
    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping
    @Transactional(rollbackOn = Exception.class)
    public ResponseEntity<String> createFilm(@RequestBody FilmDTO dto) {
        Film film = facade.mapperStruct.film.toEntity(dto);
        film.setVotes(0L);
        facade.repository.film.save(film);
        return ResponseEntity.ok("Film created!");
    }

    @ApiOperation(value = "Voting system; there is no user voting limit at the moment , but there is user role authentication.")
    @PostMapping("/vote/{id}")
    @Transactional(rollbackOn = Exception.class)
    public ResponseEntity<String> createFilm(@PathVariable("id") Long id, @RequestParam("star") RatingEnum star) {
       if(!hasAdminAuthority()){
           Optional<Film> film = facade.service.film.findById(id);

           if(film.isPresent()){
               Film filmDb = film.get();
               filmDb.setVotes(filmDb.getVotes() + star.ordinal());
               facade.repository.film.save(filmDb);
               return ResponseEntity.ok("Vote Computated!");
           }


           return ResponseEntity.notFound().build();
       }
       return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Admins cant vote!");

    }

    @ApiOperation(value = "Get Request to list all films accordingly to the used filter")
    @GetMapping()
    @Transactional(rollbackOn = Exception.class)
    public List<FilmDTO> getFilms(FilmFilterDTO filter) {
        List<FilmDTO> film  = facade.service.film.findFilmByFilter(filter);
        return film;
    }


}
