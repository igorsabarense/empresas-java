package br.com.ioasys.igorsabarense.resource;

import br.com.ioasys.igorsabarense.dto.ApplicationUserDTO;
import br.com.ioasys.igorsabarense.entity.ApplicationUser;
import br.com.ioasys.igorsabarense.entity.Role;
import br.com.ioasys.igorsabarense.service.facade.Facade;
import com.google.common.collect.Sets;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserResource {

    @Autowired
    private Facade facade;

    private Boolean hasAdminAuthorithy(){
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString().contains("ADMIN");
    }

    @ApiOperation(value = "Sign-up feature, after signing-up you should make a request to localhost:8080/login so you can get your bearer token")
    @PostMapping("/sign-up")
    public ResponseEntity<String> signUp(@RequestBody ApplicationUserDTO dto) {
        String duplicate = facade.repository.user.findByUsername(dto.getUsername()) != null ? "Username" : facade.repository.user.findByEmail(dto.getEmail()) != null ? "Email" : null;
        if(duplicate != null){
           return ResponseEntity.badRequest().body(duplicate.concat(" already in use!"));
        }else{
            ApplicationUser entity = facade.mapperStruct.user.toEntity(dto);
            Role role = facade.repository.role.findByName("USER");
            entity.setCreationDateTime(LocalDateTime.now());
            entity.setActive(true);
            entity.setRoles(Sets.newHashSet(role));
            entity.setPassword(facade.bCryptPasswordEncoder.encode(entity.getPassword()));
            facade.repository.user.save(entity);
        }

        return ResponseEntity.ok(dto.getUsername().concat(" created!"));
    }

    @ApiOperation(value = "User Update")
    @PutMapping(value = "/{id}")
    public ResponseEntity<String> updateUser(@PathVariable("id") Long id , @RequestBody ApplicationUserDTO dto) {
        ApplicationUser userDB = facade.repository.user.getOne(id);
        if(userDB != null){
            ApplicationUser retorno = facade.service.user.updateUser(userDB, dto, hasAdminAuthorithy());
            if(retorno != null){
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("No authority to update administrator");
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "User Deletion")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long id) {
        ApplicationUser userDB = facade.repository.user.getOne(id);
        if(userDB != null){
            ApplicationUser retorno = facade.service.user.deleteUser(userDB, hasAdminAuthorithy());
            if(retorno != null){
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("No authority to delete administrator");
        }
        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Get all users list")
    @GetMapping
    public Set<ApplicationUserDTO> getAllUsers(Pageable pageable) {
        Set<ApplicationUser> users = facade.service.user.getAllUsers();
        return facade.mapperStruct.user.toDto(users);
    }
}
