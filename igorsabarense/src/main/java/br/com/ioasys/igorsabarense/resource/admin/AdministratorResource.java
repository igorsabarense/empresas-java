package br.com.ioasys.igorsabarense.resource.admin;


import br.com.ioasys.igorsabarense.dto.ApplicationUserDTO;
import br.com.ioasys.igorsabarense.entity.ApplicationUser;
import br.com.ioasys.igorsabarense.entity.Role;
import br.com.ioasys.igorsabarense.service.facade.Facade;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@PreAuthorize("hasAuthority('ADMIN')")
@RestController
@RequestMapping("/admin")
public class AdministratorResource {

    @Autowired
    private Facade facade;


    @GetMapping(value = "/users/active-users")
    public List<ApplicationUserDTO> getNonAdminUsers(Pageable pageable) {
        Page<ApplicationUser> adminUsers = facade.service.user.getActiveNonAdminUsers(pageable);
        return facade.mapperStruct.user.toDto(new ArrayList<>(adminUsers.getContent()));
    }

    @PostMapping(value = "/users/")
     public ResponseEntity<String> createAdmin(@RequestBody ApplicationUserDTO dto) {
        String duplicate = facade.repository.user.findByUsername(dto.getUsername()) != null ? "Username" : facade.repository.user.findByEmail(dto.getEmail()) != null ? "Email" : null;
        if(duplicate != null){
            return ResponseEntity.badRequest().body(duplicate.concat(" already in use!"));
        }else{
            ApplicationUser entity = facade.mapperStruct.user.toEntity(dto);
            Role role = facade.repository.role.findByName("ADMIN");
            entity.setCreationDateTime(LocalDateTime.now());
            entity.setActive(true);
            entity.setRoles(Sets.newHashSet(role));
            entity.setPassword(facade.bCryptPasswordEncoder.encode(entity.getPassword()));
            facade.repository.user.save(entity);
        }

        return ResponseEntity.ok(String.format("Administrator %s created!",dto.getName()));
    }

    @PutMapping(value = "/users/{id}")
    public ResponseEntity<String> updateAdmin(@PathVariable("id") Long id , @RequestBody ApplicationUserDTO dto) {
        ApplicationUser userDB = facade.repository.user.getOne(id);
       if(userDB != null){
           ApplicationUser retorno = facade.service.user.updateAdmin(userDB, dto);
           if (retorno != null ) return ResponseEntity.ok().build();
           return ResponseEntity.badRequest().body("Username/Email already exists!");
       }
       return ResponseEntity.notFound().build();
    }

    @DeleteMapping(value = "/users/{id}")
    public ResponseEntity<String> deleteAdmin(@PathVariable("id") Long id) {
        ApplicationUser userDB = facade.repository.user.getOne(id);
        if(userDB != null){
            facade.service.user.deleteAdmin(userDB);
            return ResponseEntity.ok().build();

        }
        return ResponseEntity.notFound().build();
    }

}