package br.com.ioasys.igorsabarense.service;

import br.com.ioasys.igorsabarense.dto.FilmDTO;
import br.com.ioasys.igorsabarense.dto.FilmFilterDTO;
import br.com.ioasys.igorsabarense.entity.Film;
import br.com.ioasys.igorsabarense.enumeration.CastRoleEnumeration;
import br.com.ioasys.igorsabarense.enumeration.GenreEnumeration;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;


public interface FilmService {

    List<Film> getFilmByPersonNameRole(String name , CastRoleEnumeration role);
    List<Film> getAllFilms(Pageable pageable);
    List<Film> getFilmByGenre(GenreEnumeration genre);
    Film getFilmByTitle(String title);
    Optional<Film> findById(Long id);

    List<FilmDTO> findFilmByFilter(FilmFilterDTO filter);

    List<FilmDTO> findAllOrderByTitleAsc();
}
