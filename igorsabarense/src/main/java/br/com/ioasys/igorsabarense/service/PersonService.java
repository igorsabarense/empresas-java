package br.com.ioasys.igorsabarense.service;

import br.com.ioasys.igorsabarense.entity.Person;

import java.util.List;


public interface PersonService {

    List<Person> getAllActors();

}
