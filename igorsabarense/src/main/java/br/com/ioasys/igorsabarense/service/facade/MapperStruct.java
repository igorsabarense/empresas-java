package br.com.ioasys.igorsabarense.service.facade;

import br.com.ioasys.igorsabarense.service.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MapperStruct {

    @Autowired
    public ApplicationUserMapper user;

    @Autowired
    public RoleMapper role;

    @Autowired
    public PersonMapper person;

    @Autowired
    public FilmMapper film;


}
