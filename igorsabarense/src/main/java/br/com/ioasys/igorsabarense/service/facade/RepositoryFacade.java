package br.com.ioasys.igorsabarense.service.facade;

import br.com.ioasys.igorsabarense.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryFacade {
    @Autowired
    public ApplicationUserRepository user;

    @Autowired
    public RoleRepository role;

    @Autowired
    public PersonRepository person;

    @Autowired
    public FilmRepository film;

}
