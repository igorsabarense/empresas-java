package br.com.ioasys.igorsabarense.service.facade;

import br.com.ioasys.igorsabarense.service.ApplicationUserService;
import br.com.ioasys.igorsabarense.service.FilmService;
import br.com.ioasys.igorsabarense.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceFacade {
    @Autowired
    public ApplicationUserService user;

    @Autowired
    public FilmService film;

    @Autowired
    public PersonService person;


}
