package br.com.ioasys.igorsabarense.service.impl;

import br.com.ioasys.igorsabarense.dto.ApplicationUserDTO;
import br.com.ioasys.igorsabarense.entity.ApplicationUser;
import br.com.ioasys.igorsabarense.service.ApplicationUserService;
import br.com.ioasys.igorsabarense.service.facade.Facade;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class ApplicationUserServiceImpl implements ApplicationUserService {
    @Autowired
    private Facade facade;

    @Override
    public Set<ApplicationUser> getAllUsers() {
        Set<ApplicationUser> users = new HashSet<>(facade.repository.user.findAll());
        return users;
    }

    @Override
    public Page<ApplicationUser> getActiveNonAdminUsers(Pageable pageable) {
       return facade.repository.user.getActiveNonAdminUsers(pageable);
    }


    @Override
    public ApplicationUser updateUser(ApplicationUser userDB, ApplicationUserDTO dto, Boolean hasAdminAuthority) {

        if(validateUsernameEmail(userDB, dto)){
            ApplicationUser entityUpdate = facade.mapperStruct.user.toEntity(dto);
            if(!hasAdminAuthority && !userDB.getRoles().toString().contains("ADMIN")){
                entityUpdate.setRoles(Sets.newHashSet(facade.repository.role.findByName("USER")));
                entityUpdate.setId(userDB.getId());
                entityUpdate.setActive(entityUpdate.getActive() != null ? entityUpdate.getActive() : userDB.getActive());
                entityUpdate.setCreationDateTime(userDB.getCreationDateTime());
                entityUpdate.setPassword(facade.bCryptPasswordEncoder.encode(entityUpdate.getPassword()));
                facade.repository.user.save(entityUpdate);
                return entityUpdate;
            }else if(hasAdminAuthority){
                entityUpdate.setId(userDB.getId());
                entityUpdate.setActive(entityUpdate.getActive() != null ? entityUpdate.getActive() : userDB.getActive());
                entityUpdate.setCreationDateTime(userDB.getCreationDateTime());
                entityUpdate.setPassword(facade.bCryptPasswordEncoder.encode(entityUpdate.getPassword()));
                facade.repository.user.save(entityUpdate);
                return entityUpdate;
            }

        }
        return null;
    }

    @Override
    public ApplicationUser updateAdmin(ApplicationUser userDB, ApplicationUserDTO dto) {

        if(validateUsernameEmail(userDB, dto)){
            ApplicationUser entityUpdate = facade.mapperStruct.user.toEntity(dto);
            entityUpdate.setId(userDB.getId());
            entityUpdate.setActive(entityUpdate.getActive() != null ? entityUpdate.getActive() : userDB.getActive());
            entityUpdate.setCreationDateTime(userDB.getCreationDateTime());
            entityUpdate.setPassword(facade.bCryptPasswordEncoder.encode(entityUpdate.getPassword()));
            entityUpdate = facade.repository.user.save(entityUpdate);
            return entityUpdate;

        }
        return null;

    }

    @Override
    public ApplicationUser deleteUser(ApplicationUser user, Boolean hasAdminAuthority) {

        if(!hasAdminAuthority && !user.getRoles().toString().contains("ADMIN")){
            user.setActive(false);
            facade.repository.user.save(user);
            return user;
        }else if(hasAdminAuthority){
            user.setActive(false);
            facade.repository.user.save(user);
            return user;
        }

        return null;
    }

    public ApplicationUser deleteAdmin(ApplicationUser user) {
            user.setActive(false);
            facade.repository.user.save(user);
            return user;
    }

    private Boolean validateUsernameEmail(ApplicationUser userDB, ApplicationUserDTO dto) {
        Boolean valid = true;
        if(!userDB.getUsername().equals(dto.getUsername())){
            ApplicationUser userValidation = facade.repository.user.findByUsername(dto.getUsername());
            if(userValidation != null){
                valid = false;
            }
        }

        if(!userDB.getEmail().equals(dto.getEmail())){
            ApplicationUser userValidation = facade.repository.user.findByEmail(dto.getEmail());
            if(userValidation != null){
                valid = false;
            }
        }

        return valid;
    }


}
