package br.com.ioasys.igorsabarense.service.impl;

import br.com.ioasys.igorsabarense.entity.Person;
import br.com.ioasys.igorsabarense.service.PersonService;
import br.com.ioasys.igorsabarense.service.facade.Facade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private Facade facade;

    @Override
    public List<Person> getAllActors() {
        return facade.repository.person.findAll();
    }
}
