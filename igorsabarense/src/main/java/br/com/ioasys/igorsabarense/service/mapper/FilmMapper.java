package br.com.ioasys.igorsabarense.service.mapper;

import br.com.ioasys.igorsabarense.dto.FilmDTO;
import br.com.ioasys.igorsabarense.entity.ApplicationUser;
import br.com.ioasys.igorsabarense.entity.Film;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {PersonMapper.class})
public interface FilmMapper extends EntityMapper<FilmDTO, Film>  {
    @Override
    @Mapping(source = "person", target = "cast" )
    @Mapping(target="averageVote", expression = "java(entity.getAverageVote())")
    FilmDTO toDto(Film entity);

    @Override
    @Mapping(source = "cast" , target="person")
    Film toEntity(FilmDTO dto);

    default ApplicationUser fromId(Long id){
        if(id == null){
            return null;
        }
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setId(id);
        return applicationUser;
    }
}
