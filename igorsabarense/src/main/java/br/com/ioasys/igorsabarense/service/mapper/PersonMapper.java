package br.com.ioasys.igorsabarense.service.mapper;

import br.com.ioasys.igorsabarense.dto.PersonDTO;
import br.com.ioasys.igorsabarense.entity.Person;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", uses = {FilmMapper.class})
public interface PersonMapper extends EntityMapper<PersonDTO, Person>{
    @Override
    PersonDTO toDto(Person entity);

    @Override
    Person toEntity(PersonDTO dto);

    default Person fromId(Long id){
        if(id == null){
            return null;
        }
        Person person = new Person();
        person.setId(id);
        return person;
    }
}

