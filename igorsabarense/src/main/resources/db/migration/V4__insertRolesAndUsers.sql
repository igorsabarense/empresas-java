INSERT INTO `roles` (`name`) VALUES ('USER');
INSERT INTO `roles` (`name`) VALUES ('ADMIN');

INSERT INTO application_user
(id, active, creation_date_time, username, password, name, email)
VALUES(1, 1, '2021-01-22', 'admin', '$2a$10$yN3A4n7f17I8ckD2Sm.cEOO5BUfPHN2nyeEWwafFiF2FlM5qrsElm', 'admin', 'admin@localhost.com.br');

INSERT INTO application_user
(id, active, creation_date_time, username, password, name, email)
VALUES(2, 1, '2021-01-22', 'igorsabarense', '$2a$10$yN3A4n7f17I8ckD2Sm.cEOO5BUfPHN2nyeEWwafFiF2FlM5qrsElm', 'Igor Sabarense', 'user@localhost.com.br');
