CREATE TABLE `film_cast` (
  `film_id` bigint(20) NOT NULL,
  `person_id` bigint(20) NOT NULL,
  CONSTRAINT `film_cast_fk` FOREIGN KEY (`film_id`) REFERENCES `film` (`id`),
  CONSTRAINT `person_cast_fk` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
);
